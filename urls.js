module.exports = {
  local: "http://localhost:3000/",
  prod: "https://ch8-prod.herokuapp.com/",
  qa: "https://ch8-qa.herokuapp.com/",
};
