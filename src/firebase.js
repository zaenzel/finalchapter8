import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAf1B-eIVp8KFoJvpNew0BoiJBwVOmJm3w",
  authDomain: "bcrch7.firebaseapp.com",
  projectId: "bcrch7",
  storageBucket: "bcrch7.appspot.com",
  messagingSenderId: "722473683886",
  appId: "1:722473683886:web:56a60fb3e5b3c4d70b963f"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth()
export const storage = getStorage(app);