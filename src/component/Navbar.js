import React, { useState, useEffect, useContext } from "react";
import { Avatar, Box, Container, Stack, Typography } from "@mui/material";
import NavLinks from "./Navlinks";
import Logo from "./Logo";
import { useNavigate } from "react-router-dom";
import Button from "./Button";
// import { data } from "../App";


const Navbar = () => {
  const navigate = useNavigate();
  // const {user, setUser} = useContext(data);
  const [user, setUser] = useState()

  useEffect(() => {
    const getUser = () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("Authentication failed");
        })
        .then((response) => {
          setUser(response.user);
          console.log(response.user);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUser();
  }, []);

  return (
    <>
      <Container>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          py={3}
        >
          <Logo />
          <Stack
            direction="row"
            spacing={4}
            display={{ xs: "none", sm: "flex" }}
            alignItems="center"
          >
            <NavLinks />

            {user ? (
              <>
                <Avatar alt="userpicture" src={user.hasOwnProperty('photos') ? user.photos[0].value : null} />
                <Typography>{user.displayName || user.email}</Typography>
                <Button
                  onClick={() =>
                    window.open("http://localhost:5000/auth/logout", "_self")
                  }
                >
                  <Typography
                    variant="body"
                    fontWeight={700}
                    textTransform="capitalize"
                  >
                    Logout
                  </Typography>
                </Button>
              </>
            ) : (
              <>
                <Button onClick={() => navigate("/login")}>
                  <Typography
                    variant="body"
                    fontWeight={700}
                    textTransform="capitalize"
                  >
                    Login
                  </Typography>
                </Button>
              </>
            )}
          </Stack>
        </Box>
      </Container>
    </>
  );
};

export default Navbar;
