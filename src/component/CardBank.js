import { Typography, Box, Stack, Paper } from "@mui/material";
import {} from "@mui/system";
import React from "react";

const CardBank = () => {
  return (
    <>
      <Paper elevation={1}>
        <Stack spacing={3} p={2}>
          <Typography variant="title">Pilih Bank Transfer</Typography>
          <Typography variant="body">
            Kamu bisa membayar dengan transfer melalui ATM, Internet Banking
            atau Mobile Banking
          </Typography>
          <Box sx={{ display: "flex", alignItems: "center", gap: 3 }}>
            <Box
              sx={{
                border: "1px solid black",
                p: 1,
                width: "50px",
                display: "flex",
                justifyContent: "center",
              }}
            >
              BCA
            </Box>
            <Typography>BCA Transfer</Typography>
          </Box>
          <hr color="#EEEEEE" />
          <Box sx={{ display: "flex", alignItems: "center", gap: 3 }}>
            <Box
              sx={{
                border: "1px solid black",
                p: 1,
                width: "50px",
                display: "flex",
                justifyContent: "center",
              }}
            >
              BNI
            </Box>
            <Typography>BNI Transfer</Typography>
          </Box>
          <hr color="#EEEEEE" />
          <Box sx={{ display: "flex", alignItems: "center", gap: 3 }}>
            <Box
              sx={{
                border: "1px solid black",
                p: 1,
                width: "50px",
                display: "flex",
                justifyContent: "center",
              }}
            >
              Mandiri
            </Box>
            <Typography>Mandiri Transfer</Typography>
          </Box>
        </Stack>
      </Paper>
    </>
  );
};

export default CardBank;
