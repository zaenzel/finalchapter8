import React, { useEffect } from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { getAllCars, getCars } from "../redux/carSlice";
import { useSelector, useDispatch } from "react-redux";
import CarCard from "./CarCard";
import LinearProgress from "@mui/material/LinearProgress";

const CarListing = () => {
  const cars = useSelector(getAllCars);

  return (
    <>
      <Container>
        <Box>
          <Grid
            container
            spacing={5}
            sx={{
              display: "flex",
              justifyContent: "center",
            }}
          >
            {Object.keys(cars).length === 0 ? (
              <Box sx={{ width: "20%" }}>
                <LinearProgress />
              </Box>
            ) : (
              cars.map((car, index) => {
                return (
                  <>
                    <CarCard data={car} key={index} />
                  </>
                );
              })
            )}
          </Grid>
        </Box>
      </Container>
    </>
  );
};

export default CarListing;
