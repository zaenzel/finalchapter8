import {
  Box,
  Button,
  Container,
  Grid,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import jsPDF from "jspdf";
import carpdf from "../images/car.png";
import DownloadIcon from "@mui/icons-material/Download";

const Invoice = () => {
  const dateNow = new Date().toLocaleString();
  const pdfGenerate = () => {
    var doc = new jsPDF("landscape", "px", "a4", "false");
    doc.addImage(carpdf, "PNG", 65, 20, 500, 400);
    doc.addPage();
    doc.setFont("Helvetica", "bold");
    doc.text(60, 60, "Name : ");
    doc.text(60, 80, "Email : ");
    doc.text(60, 100, "Mobil : ");
    doc.text(60, 120, "Start : ");
    doc.text(60, 140, "Finish : ");
    doc.setFont("Helvetica", "Normal");
    doc.text(100, 60, "Jaelani");
    doc.text(100, 80, "jaelani@gmail.com");
    doc.text(100, 100, "Jeep");
    doc.text(100, 120, dateNow);
    doc.text(100, 140, "27 May 2024");
    doc.save("car.pdf");
  };

  return (
    <>
      <Stack spacing={3} mt={5}>
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Typography>Pembayaran Berhasil</Typography>
        </Box>
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Typography>
            Tunjukan Invoice Ini Kepada Petugas BCR di Titik Temu
          </Typography>
        </Box>
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Button variant="contained" onClick={pdfGenerate}>
            <DownloadIcon /> Unduh
          </Button>
        </Box>
      </Stack>
    </>
  );
};

export default Invoice;
