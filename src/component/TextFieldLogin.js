import React, { useState, useContext } from "react";
import {
  TextField,
  Typography,
  Box,
  IconButton,
  Stack,
  Link,
  Alert,
} from "@mui/material";
import Button from "./Button";
import { useNavigate } from "react-router-dom";
import { FcGoogle } from "react-icons/fc";
import { FaGithub } from "react-icons/fa";
import { AuthContext } from "../context/AuthContext";
import { auth } from "../firebase";
import { signInWithEmailAndPassword } from "firebase/auth";

const TextFieldLogin = () => {

  const navigate = useNavigate();

  const [register, setRegister] = useState(false);
  // const {user, setUser} = useContext(data);
  // const [user, setUser] = useState("")
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [eror, setEror] = useState(false);

  const { dispatch } = useContext(AuthContext);

  const onsubmit = async (e) => {
    e.preventDefault();
    // try {
    //   const res = await axios.post("http://localhost:5000/auth/login", {
    //     email,
    //     password,
    //   });
    //   dispatch({ type: "LOGIN", payload: res.data });
    //   navigate("/dashboard");
    //   // dispatch(addUser(res.data))
    // } catch (err) {
    //   console.log(err);
    // }

    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        dispatch({ type: "LOGIN", payload: user });
        navigate("/dashboard");
      })
      .catch((error) => {
        <Alert severity="error">This is an error alert — check it out!</Alert>
      });
  };

  return (
    <>
      <Box sx={{ m: 5 }}>
        <Typography component="h1" variant="h6" sx={{ mb: 5 }}>
          Welcome Admin BCR
        </Typography>
        <Typography>Email</Typography>
        <TextField
          id="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          variant="outlined"
          fullWidth
          type="email"
          role="dialog"
        />
        <Typography sx={{ mt: 3 }}>Password</Typography>
        <TextField
          id="password"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          variant="outlined"
          fullWidth
          type="password"
          role="dialogPass"
        />
        <Button
          type="submit"
          fullWidth
          sx={{ mt: 3, mb: 2 }}
          onClick={onsubmit}
          role="buttonSignin"
        >
          {register ? `Sign up` : "Login"}
        </Button>
        {eror && (
          <Box sx={{ display:"flex", alignItems:"center", justifyContent:"center"}}>
            <Typography variant="body" color="error">
              Username or Password salah
            </Typography>
          </Box>
        )}
        <Stack
          spacing={2}
          sx={{ display: "flex", alignItems: "center", mt: 2 }}
        >
          <Typography variant="body2" color="gray">
            {register ? `or sign up using` : `or login using`}
          </Typography>
          <Box>
            <IconButton>
              <FcGoogle />
            </IconButton>
            <IconButton>
              <FaGithub />
            </IconButton>
          </Box>

          {register ? (
            <Box sx={{ display: "flex" }}>
              <Typography variant="body2" color="gray" mr={1}>
                Have already account?
              </Typography>
              <Link
                component="button"
                onClick={() => {
                  setRegister(false);
                }}
              >
                LOGIN
              </Link>
            </Box>
          ) : (
            <Box sx={{ display: "flex" }}>
              <Typography variant="body2" color="gray" mr={1}>
                Need an account?
              </Typography>
              <Link
                component="button"
                onClick={() => {
                  setRegister(true);
                }}
              >
                SIGN UP
              </Link>
            </Box>
          )}
        </Stack>
      </Box>
    </>
  );
};

export default TextFieldLogin;
