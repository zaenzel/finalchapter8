import React from "react";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "./Button";
import Typography from "@mui/material/Typography";
import { RiGroupLine } from "react-icons/ri";
import { BsGear, BsJustify } from "react-icons/bs";
import { AiOutlineCalendar } from "react-icons/ai";
import { Paper } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getBtn, setButton } from "../redux/carSlice";

const CarCard = (props) => {

  const btn = useSelector(getBtn)
  const dispatch = useDispatch()
  dispatch(setButton('Pilih Mobil'))

  const navigate = useNavigate();
  const { data } = props;
  return (
    <>
      <Grid item>
        <Paper>
          <Card sx={{ width: 340 }}>
            <CardMedia
              component="img"
              height="225"
              image={data.image}
              alt="car image"
            />
            <CardContent>
              <Typography gutterBottom variant="body" component="div" mt={2}>
                {data.name}
              </Typography>
              <Typography gutterBottom variant="h6" fontWeight="bold">
                Rp {data.price.toLocaleString("id-ID")} / hari
                {/* {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(data.price)}{" "}
                / hari */}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                {data.description}
              </Typography>
              <Typography variant="body2" sx={{ mt: 2 }}>
                <RiGroupLine /> {data.passangers} Orang
              </Typography>
              <Typography variant="body2" sx={{ mt: 2 }}>
                <BsGear /> {data.fuel}
              </Typography>
              <Typography variant="body2" sx={{ mt: 2 }}>
                <AiOutlineCalendar /> Tahun {data.time}
              </Typography>
            </CardContent>
            <CardActions>
              <Button onClick={() => navigate(`/detail/${data.id}`)} fullWidth>
                {btn}
              </Button>
            </CardActions>
          </Card>
        </Paper>
      </Grid>
    </>
  );
};

export default CarCard;
