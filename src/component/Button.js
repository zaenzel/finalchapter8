import React from "react";
import { Button } from "@mui/material";

const Btn = ({children, ...props}) => {
  return (
    <>
      <Button
        size="large"
        variant="contained"
        // eslint-disable-next-line no-undef
        color="secondary"
        {...props}
      >
        {children}
      </Button>
    </>
  );
};

export default Btn;
