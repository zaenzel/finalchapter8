import { Box, Button, Container, Grid, Typography } from "@mui/material";
import React from "react";
import CardBank from "./CardBank";
import CardOrder from "./CardOrder";
import Invoice from "./Invoice";

const WraperPayment = () => {
  return (
    <>
      {/* <Container sx={{display:"flex", flexDirection:"column", gap:10}}>
      <Grid
        container
        spacing={{ xs: 3, sm: 0 }}
        sx={{ display: "flex", justifyContent: "center" }}
      >
        <Grid item md={8}>
          <CardBank/>
        </Grid>
        
        <Grid item md={4}>
          <CardOrder/>
        </Grid>
       
      </Grid>
    </Container> */}
      <Container fluid>
          <Invoice />
      </Container>
    </>
  );
};

export default WraperPayment;
