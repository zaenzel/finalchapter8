import { Box } from "@mui/material";
import React from "react";
import Hero from "./Hero";
import Navbar from "./Navbar";

const Header = () => {
  return (
    <>
      <Box sx={{backgroundColor: "#F1F3FF"}}>
        <Navbar />
        <Hero />
      </Box>
    </>
  );
};

export default Header;
