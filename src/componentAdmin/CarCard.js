import React, { useState } from "react";
import Car from "../images/modal.png";
import {
  Button,
  Paper,
  Typography,
  CardMedia,
  CardContent,
  CardActions,
  Grid,
  Card,
  Modal,
  Box,
  Stack,
} from "@mui/material";
import { FiKey } from "react-icons/fi";
import { BiTimeFive } from "react-icons/bi";
import DeleteIcon from "@mui/icons-material/Delete";
import { FiEdit } from "react-icons/fi";
import { doc, deleteDoc } from "firebase/firestore";
import { db } from "../firebase";
import { useNavigate } from "react-router-dom";
import imgModal from "../images/modal.png";
import styled from "@emotion/styled";

const CarCard = ({ data }) => {
  const WrapModal = styled(Box)(({ theme }) => ({
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    backgroundColor: "white",
    border: "0.5px solid #000",
    boxShadow: 24,
    display: "flex",
    justifyContent: "center",
  }));

  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleDelete = async (id) => {
    try {
      await deleteDoc(doc(db, "cars", id));
    } catch (error) {
      console.log(error);
    }
    setOpen(false);
  };

  console.log(data.img);
  return (
    <>
      <Grid item>
        <Paper>
          <Card sx={{ maxWidth: 300, minWidth: 300 }}>
            <CardMedia
              component="img"
              height="225"
              image={data.img}
              alt="car image"
            />
            <CardContent>
              <Typography gutterBottom variant="title" component="div" mt={2}>
                {data.name}
              </Typography>
              <Typography gutterBottom variant="h6" fontWeight="bold">
                {new Intl.NumberFormat("id-ID", {
                  style: "currency",
                  currency: "IDR",
                }).format(data.price)}{" "}
                / hari
              </Typography>
              <Typography variant="body2" sx={{ mt: 2 }}>
                <FiKey fontSize="large" /> Start Rent - Finish Rent
              </Typography>
              <Typography variant="body2" sx={{ mt: 2 }}>
                <BiTimeFive fontSize="large" /> {data.update}
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                variant="outlined"
                color="error"
                startIcon={<DeleteIcon />}
                fullWidth
                onClick={handleOpen}
              >
                Delete
              </Button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <WrapModal>
                  <Box
                    flexDirection="column"
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyItems: "center",
                      p: 5,
                    }}
                  >
                    <img
                      src={imgModal}
                      alt="gambar modal"
                      width="50%"
                      height="50%"
                    />
                    <Typography
                      variant="title"
                    >
                      Menghapus Data Mobil
                    </Typography>
                    <Typography variant="body" mt={2} ml={5}>
                      Setelah dihapus, data mobil tidak dapat dikembalikan.Yakin
                      ingin menghapus?
                    </Typography>
                    <Box sx={{ display: "flex", gap: 2, mt: 2 }}>
                      <Button
                        variant="contained"
                        onClick={(e) => {
                          setOpen(false);
                        }}
                      >
                        Cancel
                      </Button>
                      <Button
                        variant="contained"
                        color="error"
                        onClick={(e) => {
                          handleDelete(data.id);
                        }}
                      >
                        Delete
                      </Button>
                    </Box>
                  </Box>
                </WrapModal>
              </Modal>
              <Button
                variant="contained"
                color="success"
                startIcon={<FiEdit />}
                fullWidth
                onClick={(e) => navigate(`update/${data.id}`)}
              >
                Edit
              </Button>
            </CardActions>
          </Card>
        </Paper>
      </Grid>
    </>
  );
};

export default CarCard;
