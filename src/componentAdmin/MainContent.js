import { Container, Typography } from "@mui/material";
import React from "react";
import ListCar from "./ListCar";

const MainContent = () => {
  return (
    <>
      <Container>
          <ListCar />
      </Container>
    </>
  );
};

export default MainContent;
