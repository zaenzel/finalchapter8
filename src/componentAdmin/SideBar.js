import {
  Box,
  Grid,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import DirectionsCarFilledIcon from "@mui/icons-material/DirectionsCarFilled";
import RectangleIcon from "@mui/icons-material/Rectangle";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const SideBar = () => {
  const StyledSidebar = styled(Box)(({ theme }) => ({
    flex:0,
    display: "block",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
    backgroundColor: theme.palette.primary.main,
    minHeight: "100vh",
  }));

  const Icons = styled(Grid)(({ theme }) => ({
    gap: "5px",
    color: "white",
    marginTop: 40,
    padding:5
  }));

  return (
    <StyledSidebar>
      <Grid container sx={{ justifyContent: "center", marginTop: 3 }}>
        <RectangleIcon fontSize="large" htmlColor="white" />
        <Link to="/dashboard">
          <Icons container component="a" href="#dashboard">
            <Grid item xs={12} display="flex" justifyContent="center">
              <HomeOutlinedIcon fontSize="large" />
            </Grid>
            <Grid item xs={12} display="flex" justifyContent="center">
              <Typography variant="body">Dashboard</Typography>
            </Grid>
          </Icons>
        </Link>
        <Link to="/dashboard/listcar">
          <Icons container component="a" href="#list-car">
            <Grid item xs={12} display="flex" justifyContent="center">
              <DirectionsCarFilledIcon fontSize="large" />
            </Grid>
            <Grid item xs={12} display="flex" justifyContent="center">
              <Typography variant="body">List Car</Typography>
            </Grid>
          </Icons>
        </Link>
      </Grid>
    </StyledSidebar>
  );
};

export default SideBar;
