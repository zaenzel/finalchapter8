import { Container, Stack, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

const Page404 = () => {
  return (
    <div className="404">
      <Container
        sx={{
          display: "flex",
          justifyContent: "center",
          width: "100v",
          height: "100vh",
          alignItems: "center",
        }}
      >
        <Stack>
          <Typography variant="h1">404</Typography>
          <Link to={"/"}>
            <Typography>BACK TO MAIN PAGE</Typography>
          </Link>
        </Stack>
      </Container>
    </div>
  );
};

export default Page404;
