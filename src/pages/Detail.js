import React from 'react'
import DetailWraper from '../component/DetailWraper'
import Footer from '../component/Footer'
import HeaderDetail from '../component/HeaderDetail'
import Search from '../component/Search'

const Detail = () => {
  return (
    <>
    <HeaderDetail />
    <Search />
    <DetailWraper />
    <Footer />
    </>
  )
}

export default Detail