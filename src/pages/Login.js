import React from "react";
import { Grid } from "@mui/material";
import Car from "../images/angkot.jpg";
import TextFieldLogin from "../component/TextFieldLogin";

const Login = () => {
  return (
    <>
      <Grid
        Container
        sx={{ height: "100vh", display: "flex" }}
        justifyContent="center"
        alignItems="center"
      >
        <Grid item md={6} xs={false} sm={4} sx={{}}>
          <img src={Car} alt="car" style={{ width: "70vw", height: "100vh" }} />
        </Grid>
        <Grid item md={6} xs={12} sm={8}>
          <TextFieldLogin />
        </Grid>
      </Grid>
    </>
  );
};

export default Login;
