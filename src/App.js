import React, { useContext } from "react";
import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import Main from "./pages/Main";
import Detail from "./pages/Detail";
import Page404 from "./pages/Page404";
import Login from "./pages/Login";
import Payment from "./pages/Payment";
import Dashboard from "./pages/Dashboard";
import ListCar from "./pages/ListCar";
import { AuthContext } from "./context/AuthContext";
import AddCar from "./pages/AddCar";
import Update from "./pages/Update";
// export const data = createContext();

function App() {
  const { currentUser } = useContext(AuthContext);

  const RequireAuth = ({ children }) => {
    return currentUser ? children : <Navigate to="/login" />;
  };

  return (
    <div className="App">
      <Routes>
        <Route path="/">
          <Route index element={<Main />} />
          <Route path="login" element={<Login />} />
          <Route path="detail/:id" element={<Detail />} />
          <Route path="payment/:id" element={<Payment />} />

          <Route path="dashboard">
            <Route
              index
              element={
                <RequireAuth>
                  <Dashboard />
                </RequireAuth>
              }
            />
            <Route path="listcar" element={<ListCar />} />
            <Route path="listcar/addcar" element={<AddCar />} />
            <Route path="listcar/update/:id" element={<Update />} />
          </Route>
          <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
