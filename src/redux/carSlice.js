import {
  createSlice,
  createAsyncThunk,
} from "@reduxjs/toolkit";
import api from "../api/api";

export const fetchAsyncCars = createAsyncThunk(
  "cars/fetchAsyncCars",
  async (dataFilter) => {
    const res = await api.get("/cars");
    const data = res.data;
    const selectedData = () => {
      return data.filter((mobil) => {
        return mobil.name.includes(dataFilter.mobil);
      });
    };
    return selectedData();
  }
);

export const fetchAsyncCarsDetail = createAsyncThunk(
  "cars/fetchAsyncCarsDetail",
  async (id) => {
    const res = await api.get(`/cars/${id}`);
    return res.data;
  }
);

const initialState = {
  cars: {},
  carsDetail: {},
  btn: "",
};

const carSlice = createSlice({
  name: "cars",
  initialState,
  reducers: {
    // addCars: (state, { payload }) => {
    //   state.cars = payload;
    // },
    setButton: (state, { payload }) => {
      state.btn = payload;
    },
    removeDetailCar: (state) => {
      state.carsDetail = {};
    },
  },
  extraReducers: {
    [fetchAsyncCars.pending]: () => {
      console.log("pending");
    },
    [fetchAsyncCars.fulfilled]: (state, { payload }) => {
      console.log("success");
      return { ...state, cars: payload };
      //   carEntity.setAll(state, payload)
    },
    [fetchAsyncCars.rejected]: () => {
      console.log("rejected");
    },
    [fetchAsyncCarsDetail.fulfilled]: (state, { payload }) => {
      console.log("success");
      return { ...state, carsDetail: payload };
    },
  },
});

export const { removeDetailCar, addCars, setButton } = carSlice.actions;
export const getAllCars = (state) => state.cars.cars;
export const getDetailCar = (state) => state.cars.carsDetail;
export const getBtn = (state) => state.cars.btn;
export default carSlice.reducer;

/*
const filterCars = (cars, queries) => {
  return cars.filter((car) => {
  return (
  car.name.toLowerCase().includes(queries.carName.toLowerCase()) &&
  car.category
  .toLowerCase()
  .includes(queries.carType === "all" ? "" : queries.carType) &&
  (queries.minPrice == 0
  ? true
  : parseInt(car.price) >= queries.minPrice) &&
  (queries.maxPrice == 0 ? true : parseInt(car.price) <= queries.maxPrice)
  );
  });
  };
  export default filterCars; 
*/
