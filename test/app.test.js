const { assert } = require("chai");

describe("React application home page", () => {
  it("veryfy that the applinks says Learn React", () => {
    browser.url("/");
    let text = $(
      ".MuiTypography-root MuiTypography-body MuiLink-root MuiLink-underlineNone css-43afkd-MuiTypography-root-MuiLink-root"
    ).getText();
    assert.equal(text, "Our Service");
  });
});
